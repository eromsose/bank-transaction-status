document.getElementById('transactionForm').addEventListener('submit', function(event) {
    event.preventDefault(); 
  
    const email = document.getElementById('email').value;
    const amount = document.getElementById('amount').value;
  
    const url = 'https://api.kochure.com/v1/sup/bank-withdrawals/re-initiate';
  
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ email: email, amount: amount })
    })
    .then(response => {
        console.log(response)
      if (!response.ok) {
        throw new Error('Network response was not ok ' + response.statusText);
      }
      response.json().then(async(returnedData)=>{
        hello(returnedData)
      });
    })
    .then(data => {
        
    })
    .catch(error => {
      const statusDiv = document.getElementById('status');
      statusDiv.innerHTML = `<p>There has been a problem with your fetch operation: ${error.message}</p>`;
    });
  });
  function hello(returnedData){
    const aa = document.getElementById("email-1");
    aa.innerText = `Email is: ${returnedData.data.transaction.email}`;
    const statusDiv = document.getElementById('status');
    statusDiv.innerText = `<p>Transaction Status: ${returnedData.data.transaction.status}</p>`;
        
        document.getElementById("amount-2").innerText = `${returnedData.data.transaction.amount}`;  
        document.getElementById("reference").innerText= `${returnedData.data.transaction.reference}`
  }